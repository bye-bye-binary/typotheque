#!/usr/bin/env python3

# import subprocess
import bottle
import os
import featuring_module as feat

@bottle.get("/<filepath:path:>")
def staticzzz(filepath):
    return bottle.static_file(filepath, root="./")

@bottle.get('/')
def login_form():
    return bottle.template('index.html')

@bottle.post('/bzz')
def submit_form():
    # file = bottle.request.files.font
    font = bottle.request.files.get('font')
    name, ext = os.path.splitext(font.filename)
    if ext not in ('.ttf','.otf','.ufo'):
        return 'Mauvais format de fichier :( essaie plutôt en .otf ou .ufo ou .ttf :)'

    save_path = './'
    if not os.path.isfile('./%s' % font.filename):
        font.save(save_path) # appends font.filename automatically

    feat.qunifying('%s' % font.filename)
    fontname = font.filename.split('.')

    return bottle.template('bzz.tpl', file=fontname[-2], ext=fontname[-1])

bottle.run(bottle.app(), host='0.0.0.0', port=8000, debug=True, reload=True)

# bottle.run(bottle.app(), host='localhost', port=5000, debug=True, reload=True)

