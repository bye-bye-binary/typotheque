# Ce fichier de features documente les prérequis et l’écriture des features OpenType recommandées par la Queer Unicode Initiative de la collective Bye Bye Binary. http://genderfluid.space
# Les contenus de ce fichier sont à mettre dans le fichier features.fea qui se trouve dans le dossier ufo, à la racine
# Pour le rendre fonctionnel dans votre fonte, il faut que cette fonte contienne les 6 glyphes suivantes :
# — un point médian ‘periodcentered’ U+00B7
# — un point médian ajusté à hauteur des capitales — pour cela ajouter un glyphe nommé ‘periodcentered.case’ Hupupup’ tu peux par exemple copier coller ton médian standard et le remonter un poil!
# — un point de suspension aka ‘ellipsis’ U+2026
# — une minuscule latine ē ‘emacron’ U+0113
# — une capitale latine Ē ‘Emacron’ U+0112

# Les ē et Ē macron nous seront utiles pour marquer un rappel de l’accent grave dans les mots avec terminaison en ère comme boulanger·e → boulangēre ou ouvrier·e → ouvriēre.

# Les features fonctionnent en cascade donc l’ordre dans lequel elles sont rédigées a de l’importance.
# Par exemple: on remplace d’abord les points médians en cohabitation avec des capitales par des points médians de capitales ‘periodcentered.case’ avant de lister des remplacement qui font appel à ce même ‘periodcentered.case’ (ex: sub A periodcentered.case E by A_E;)

# Pour les terminaisons en ‘eur/rice’ nous avons opté pour une rédaction en “eur·ice” pour laquelle nous recommandons l’emploi d’une ligature ‘r_i’ U+F4570 (et ‘R_I’ U+F4470).

#♥#♥#

# LE CODE COMMENCE ICI, avec des commentaires avant chaque commande:

# on met toutes les capitales dans une classe:
@Uppercase = [A Acircumflex Adieresis Agrave B C Ccedilla D E Eacute Ecircumflex Edieresis Egrave F G H I Icircumflex Idieresis J K L M N O Ocircumflex Odieresis OE P Q R S T U Ucircumflex Udieresis Ugrave V W X Y Z W_W_W Enb Afem Amasc Cfem Cmasc Efem Emasc Ffem Fmasc Hfem Lfem Lmasc Nfem Omasc Qfem Rfem Rmasc Sfem Smasc Tfem Ufem Umasc Vfem Xmasc A_O A_E F_V C_H C_Q N_N R_S X_C X_S U_L U_R E_S S_L_L L_E L_U];

# on s’assure du bon passage des glyphes inclusifs de bas-de-casse à capitales:
feature case{
    sub a_o by A_O;
    sub a_e by A_E;
    sub f_v by F_V;
    sub c_h by C_H;
    sub c_q by C_Q;
    sub r_s by R_S;
    sub x_c by X_C;
    sub x_s by X_S;
    sub u_l by U_L;
    sub u_r by U_R;
    sub e_s by E_S;
    sub s_l_l by S_L_L;
    sub l_e by L_E;
    sub l_u by L_U;
} case;


# c’est parti pour les ligatures:
feature liga{

  # on s’occupe d’abord des ligatures standards (non-inclusives):
  lookup ligaturesbasiques{
      sub f i by f_i;
      sub f j by f_j;
      sub f l by f_l;
      sub f h by f_h;
      sub f k by f_k;
      sub f f by f_f;
      sub f f i by f_f_i;
      sub f f l by f_f_l;
      } ligaturesbasiques;

  # pour faciliter la saisie sans passer par le point médian, on remplace les .. par des points médians. On cherche donc toutes les suites de 2 points pour les remplacer par un point médian (et les suites de 3 points par une ellipse):
  lookup point {
      sub period period by periodcentered;
      sub period period period by ellipsis;
      } point;

  # on remplace le point médian bas-de-casse par un point médian des capitales ‘periodcentered.case’ quand il est entouré de capitales:
    lookup pointcap {
  	sub @Uppercase periodcentered' by periodcentered.case;
    sub periodcentered' @Uppercase by periodcentered.case;
 } pointcap;


  # on remplace le e ou egrave par un emacron dans les mots boulangēr·e, ouvriēr·e, sēc·he, poēte·sse
  lookup macron {
      sub e' r periodcentered by emacron;
      sub egrave' r periodcentered by emacron;
      sub E' R periodcentered.case by Emacron;
      sub Egrave' R periodcentered.case by Emacron;
    	sub e' c periodcentered h by emacron;
    	sub egrave' c periodcentered h by emacron;
    	sub E' C periodcentered.case H by Emacron;
    	sub Egrave' C periodcentered.case H by Emacron;
    	sub eacute' t e periodcentered s s by emacron;
    	sub egrave' t e periodcentered s s by emacron;
    	sub Eacute' T E periodcentered.case S S by Emacron;
    	sub Egrave' T E periodcentered.case S S by Emacron;
      } macron;

  # AJOUTEZ VOS LIGATURES INCLUSIVES ICI ♥ ♥ : (voir en haut pour l'ordre/cascade et le lien vers la syntaxe/tableau QUNI)
  lookup ligaturesinclusives {
      sub a periodcentered e by a_e;
      sub a periodcentered o by a_o;
      sub a u periodcentered agrave space l a by a_u_l_a;
      sub agrave space l a periodcentered a u by a_u_l_a;
      sub c periodcentered h by c_h;
      sub c periodcentered q by c_q;
      sub d periodcentered e by d_e;
      sub d u periodcentered d e space l a by d_u_d_e_l_a;
      sub e periodcentered a by e_a;
      sub eacute periodcentered e by eacute_e;
      sub e periodcentered t by e_t;
      sub e periodcentered s by e_s;
      sub e periodcentered u by e_u;
      sub f periodcentered f e by f_f_e;
      sub f periodcentered v by f_v;
      sub g periodcentered u by g_u;
      sub i periodcentered e by i_e;
      sub l periodcentered e by l_e;
      sub l periodcentered l by l_l;
      sub l periodcentered u by l_u;
      sub m periodcentered p by m_p;
      sub n periodcentered e by n_e;
      sub n periodcentered n by n_n;
      sub o n periodcentered a by o_f;
      sub o periodcentered idieresis by o_idieresis;
      sub o periodcentered i by o_idieresis;
      sub o periodcentered t by o_t;
      sub p periodcentered m by p_m;
      sub p periodcentered e by p_e;
      sub r periodcentered e by r_e;
      sub r periodcentered i by r_i;
      sub r periodcentered r i by r_i;
      sub r periodcentered s by r_s;
      sub s periodcentered c by s_c;
      sub s periodcentered e by s_e;
      sub s periodcentered l by s_l;
      sub s periodcentered s by s_s;
      sub s periodcentered t by s_t;
      sub t periodcentered e by t_e;
      sub t periodcentered t by t_t;
      sub u periodcentered e by u_e;
      sub u periodcentered l by u_l;
      sub x periodcentered c by x_c;
      sub x periodcentered l by x_l;
      sub x periodcentered l l by x_l_l;
      sub x periodcentered s by x_s;
      sub A periodcentered.case E by A_E;
      sub A periodcentered.case O by A_O;
      sub A U periodcentered.case Agrave space L A by A_U_L_A;
      sub Agrave space L A periodcentered.case A U by A_U_L_A;
      sub C periodcentered.case H by C_H;
      sub C periodcentered.case Q by C_Q;
      sub D periodcentered.case E by D_E;
      sub D U periodcentered.case D E space L A by D_U_D_E_L_A;
      sub E periodcentered.case A by E_A;
      sub Eacute periodcentered.case E by Eacute_E;
      sub E periodcentered.case T by E_T;
      sub E periodcentered.case S by E_S;
      sub E periodcentered.case U by E_U;
      sub F periodcentered.case F E by F_F_E;
      sub F periodcentered.case V by F_V;
      sub G periodcentered.case U by G_U;
      sub I periodcentered.case E by I_E;
      sub I periodcentered e by I_E;
      sub L periodcentered.case E by L_E;
      sub L periodcentered.case L by L_L;
      sub L periodcentered.case U by L_U;
      sub M periodcentered.case P by M_P;
      sub N periodcentered.case E by N_E;
      sub N periodcentered.case N by N_N;
      sub O N periodcentered.case A by O_F;
      sub O periodcentered.case Idieresis by O_Idieresis;
      sub O periodcentered.case I by O_Idieresis;
      sub O periodcentered.case T by O_T;
      sub P periodcentered.case M by P_M;
      sub P periodcentered.case E by P_E;
      sub R periodcentered.case E by R_E;
      sub R periodcentered.case I by R_I;
      sub R periodcentered.case R I by R_I;
      sub R periodcentered.case S by R_S;
      sub S periodcentered.case C by S_C;
      sub S periodcentered.case E by S_E;
      sub S periodcentered.case L by S_L;
      sub S periodcentered.case S by S_S;
      sub S periodcentered.case T by S_T;
      sub T periodcentered.case E by T_E;
      sub T periodcentered.case T by T_T;
      sub U periodcentered.case E by U_E;
      sub U periodcentered.case L by U_L;
      sub X periodcentered.case C by X_C;
      sub X periodcentered.case L by X_L;
      sub X periodcentered.case L L by X_L_L;
      sub X periodcentered.case S by X_S;
      } ligaturesinclusives;

} liga;


#On duplique tout le contenu de "liga" dans "rlig" (sauf les ligatures basiques) en renommant les lookups (par exemple ajouter un "2" à chaque fois).
#Cela permet aux ligatures de ne pas sauter lorsqu'on augmente l'interlettrage dans un logiciel de mise en pages. Il est nécessaire que les lignes soient présentes à la fois dans "liga" et "rlig" car tous les logiciels ne supportent pas "rlig" (mais ils supportent tous "liga").
feature rlig{

# pour faciliter la saisie sans passer par le point médian, on remplace les .. par des points médians. On cherche donc toutes les suites de 2 points pour les remplacer par un point médian (et les suites de 3 points par une ellipse):
  lookup point2 {
      sub period period by periodcentered;
      sub period period period by ellipsis;
      } point2;

  # on remplace le point médian bas-de-casse par un point médian des capitales ‘periodcentered.case’ quand il est entouré de capitales:
  	sub @Uppercase periodcentered' by periodcentered.case;
      sub periodcentered' @Uppercase by periodcentered.case;

  # on remplace le e ou egrave par un emacron dans les mots boulangēr·e, ouvriēr·e, sēc·he, poēte·sse
  lookup macron2 {
      sub e' r periodcentered by emacron;
      sub egrave' r periodcentered by emacron;
      sub E' R periodcentered.case by Emacron;
      sub Egrave' R periodcentered.case by Emacron;
    	sub e' c periodcentered h by emacron;
    	sub egrave' c periodcentered h by emacron;
    	sub E' C periodcentered.case H by Emacron;
    	sub Egrave' C periodcentered.case H by Emacron;
    	sub eacute' t e periodcentered s s by emacron;
    	sub egrave' t e periodcentered s s by emacron;
    	sub Eacute' T E periodcentered.case S S by Emacron;
    	sub Egrave' T E periodcentered.case S S by Emacron;
      } macron2;

  # on recopie ici les mêmes ligatures simplement avec un nom de lookup différent, en ajoutant un 2 après ligaturesinclusives
  lookup ligaturesinclusives2 {
      sub a periodcentered e by a_e;
      sub a periodcentered o by a_o;
      sub a u periodcentered agrave space l a by a_u_l_a;
      sub agrave space l a periodcentered a u by a_u_l_a;
      sub c periodcentered h by c_h;
      sub c periodcentered q by c_q;
      sub d periodcentered e by d_e;
      sub d u periodcentered d e space l a by d_u_d_e_l_a;
      sub e periodcentered a by e_a;
      sub eacute periodcentered e by eacute_e;
      sub e periodcentered t by e_t;
      sub e periodcentered s by e_s;
      sub e periodcentered u by e_u;
      sub f periodcentered f e by f_f_e;
      sub f periodcentered v by f_v;
      sub g periodcentered u by g_u;
      sub i periodcentered e by i_e;
      sub l periodcentered e by l_e;
      sub l periodcentered l by l_l;
      sub l periodcentered u by l_u;
      sub m periodcentered p by m_p;
      sub n periodcentered e by n_e;
      sub n periodcentered n by n_n;
      sub o n periodcentered a by o_f;
      sub o periodcentered idieresis by o_idieresis;
      sub o periodcentered i by o_idieresis;
      sub o periodcentered t by o_t;
      sub p periodcentered m by p_m;
      sub p periodcentered e by p_e;
      sub r periodcentered e by r_e;
      sub r periodcentered i by r_i;
      sub r periodcentered r i by r_i;
      sub r periodcentered s by r_s;
      sub s periodcentered c by s_c;
      sub s periodcentered e by s_e;
      sub s periodcentered l by s_l;
      sub s periodcentered s by s_s;
      sub s periodcentered t by s_t;
      sub t periodcentered e by t_e;
      sub t periodcentered t by t_t;
      sub u periodcentered e by u_e;
      sub u periodcentered l by u_l;
      sub x periodcentered c by x_c;
      sub x periodcentered l by x_l;
      sub x periodcentered l l by x_l_l;
      sub x periodcentered s by x_s;
      sub A periodcentered.case E by A_E;
      sub A periodcentered.case O by A_O;
      sub A U periodcentered.case Agrave space L A by A_U_L_A;
      sub Agrave space L A periodcentered.case A U by A_U_L_A;
      sub C periodcentered.case H by C_H;
      sub C periodcentered.case Q by C_Q;
      sub D periodcentered.case E by D_E;
      sub D U periodcentered.case D E space L A by D_U_D_E_L_A;
      sub E periodcentered.case A by E_A;
      sub Eacute periodcentered.case E by Eacute_E;
      sub E periodcentered.case T by E_T;
      sub E periodcentered.case S by E_S;
      sub E periodcentered.case U by E_U;
      sub F periodcentered.case F E by F_F_E;
      sub F periodcentered.case V by F_V;
      sub G periodcentered.case U by G_U;
      sub I periodcentered.case E by I_E;
      sub I periodcentered e by I_E;
      sub L periodcentered.case E by L_E;
      sub L periodcentered.case L by L_L;
      sub L periodcentered.case U by L_U;
      sub M periodcentered.case P by M_P;
      sub N periodcentered.case E by N_E;
      sub N periodcentered.case N by N_N;
      sub O N periodcentered.case A by O_F;
      sub O periodcentered.case Idieresis by O_Idieresis;
      sub O periodcentered.case I by O_Idieresis;
      sub O periodcentered.case T by O_T;
      sub P periodcentered.case M by P_M;
      sub P periodcentered.case E by P_E;
      sub R periodcentered.case E by R_E;
      sub R periodcentered.case I by R_I;
      sub R periodcentered.case R I by R_I;
      sub R periodcentered.case S by R_S;
      sub S periodcentered.case C by S_C;
      sub S periodcentered.case E by S_E;
      sub S periodcentered.case L by S_L;
      sub S periodcentered.case S by S_S;
      sub S periodcentered.case T by S_T;
      sub T periodcentered.case E by T_E;
      sub T periodcentered.case T by T_T;
      sub U periodcentered.case E by U_E;
      sub U periodcentered.case L by U_L;
      sub X periodcentered.case C by X_C;
      sub X periodcentered.case L by X_L;
      sub X periodcentered.case L L by X_L_L;
      sub X periodcentered.case S by X_S;
      } ligaturesinclusives2;

} rlig;
