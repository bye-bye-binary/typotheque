# Fichiers features ♥

Comme expliqué sur la page [QUNI de la typothèque Bye Bye Binary](https://typotheque.genderfluid.space/quni.html#comment-qunifier), ces fichiers d'exemples commentés contiennent l'encodage des features OpenType permettant le remplacement automatique de terminaisons genrées (ex: ami·e) en ligatures inclusives et non-binaires!

Vous pouvez les utiliser comme référence et en copier les contenus, pour les mettre dans un fichier nommé features.fea dans le dossier source de votre fonte.
