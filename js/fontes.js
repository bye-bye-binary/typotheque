$(function(){

	$("#germinal").click(function(){
		$(this).addClass("styleon"),
		$("#floreal").removeClass("styleon"),
		$("#fructidor").removeClass("styleon"),
		$("body").addClass("germinal");
		$("body").removeClass("floreal");
		$("body").removeClass("fructidor");
	});

	$("#floreal").click(function(){
		$(this).toggleClass("styleon"),
		$("#germinal").removeClass("styleon"),
		$("#fructidor").removeClass("styleon"),
		$("body").addClass("floreal");
		$("body").removeClass("germinal");
		$("body").removeClass("fructidor");
	});

	$("#fructidor").click(function(){
		$(this).addClass("styleon"),
		$("#floreal").removeClass("styleon"),
		$("#germinal").removeClass("styleon"),
		$("body").addClass("fructidor");
		$("body").removeClass("germinal");
		$("body").removeClass("floreal");
	});

	$("#regular").click(function(){
		$(this).addClass("styleon"),
		$("#bold").removeClass("styleon"),
		$("#black").removeClass("styleon"),
		$("#light").removeClass("styleon"),
		$("#italic").removeClass("styleon"),
		$("body").addClass("regular");
		$("body").removeClass("bold");
		$("body").removeClass("black");
		$("body").removeClass("light");
		$("body").removeClass("italic");
	});

	$("#bold").click(function(){
		$(this).addClass("styleon"),
		$("#regular").removeClass("styleon"),
		$("body").addClass("bold");
		$("body").removeClass("regular");
	});

	$("#black").click(function(){
		$(this).addClass("styleon"),
		$("#regular").removeClass("styleon"),
		$("#light").removeClass("styleon"),
		$("body").addClass("black");
		$("body").removeClass("regular");
		$("body").removeClass("light");
	});

	$("#light").click(function(){
		$(this).addClass("styleon"),
		$("#regular").removeClass("styleon"),
		$("#black").removeClass("styleon"),
		$("body").addClass("light");
		$("body").removeClass("regular");
		$("body").removeClass("black");
	});

	$("#italic").click(function(){
		$(this).addClass("styleon"),
		$("#regular").removeClass("styleon"),
		$("body").addClass("italic");
		$("body").removeClass("regular");
	});


	$("#ductus-regular").click(function(){
		$(this).addClass("styleon"),
		$("#calligraphic").removeClass("styleon"),
		$("#gemetric").removeClass("styleon"),
		$("body").addClass("ductus-normal");
		$("body").removeClass("calligraphic");
		$("body").removeClass("geometric");
	});

	$("#calligraphic").click(function(){
		$(this).addClass("styleon"),
		$("#ductus-regular").removeClass("styleon"),
		$("#geometric").removeClass("styleon"),
		$("body").addClass("calligraphic");
		$("body").removeClass("ductus-regular");
		$("body").removeClass("geometric");
	});

	$("#geometric").click(function(){
		$(this).addClass("styleon"),
		$("#ductus-regular").removeClass("styleon"),
		$("#calligraphic").removeClass("styleon"),
		$("body").addClass("geometric");
		$("body").removeClass("ductus-regular");
		$("body").removeClass("calligraphic");
	});

	$("#ductus-regular-mono").click(function(){
		$(this).addClass("styleon"),
		$("#calligraphic-mono").removeClass("styleon"),
		$("#gemetric-mono").removeClass("styleon"),
		$("body").addClass("ductus-normal-mono");
		$("body").removeClass("calligraphic-mono");
		$("body").removeClass("geometric-mono");
	});

	$("#calligraphic-mono").click(function(){
		$(this).addClass("styleon"),
		$("#ductus-regular-mono").removeClass("styleon"),
		$("#geometric-mono").removeClass("styleon"),
		$("body").addClass("calligraphic-mono");
		$("body").removeClass("ductus-regular-mono");
		$("body").removeClass("geometric-mono");
	});

	$("#geometric-mono").click(function(){
		$(this).addClass("styleon"),
		$("#ductus-regular-mono").removeClass("styleon"),
		$("#calligraphic-mono").removeClass("styleon"),
		$("body").addClass("geometric-mono");
		$("body").removeClass("ductus-regular-mono");
		$("body").removeClass("calligraphic-mono");
	});


	$("#poppins-regular").click(function(){
		$(this).addClass("styleon"),
		$("#poppins-italic").removeClass("styleon"),
		$("#poppins-bold").removeClass("styleon"),
		$("#poppins-bold-italic").removeClass("styleon"),
		$("body").addClass("poppins-regular");
		$("body").removeClass("poppins-italic");
		$("body").removeClass("poppins-bold");
		$("body").removeClass("poppins-bold-italic");
	});

	$("#poppins-italic").click(function(){
		$(this).addClass("styleon"),
		$("#poppins-regular").removeClass("styleon"),
		$("#poppins-bold").removeClass("styleon"),
		$("#poppins-bold-italic").removeClass("styleon"),
		$("body").addClass("poppins-italic");
		$("body").removeClass("poppins-regular");
		$("body").removeClass("poppins-bold");
		$("body").removeClass("poppins-bold-italic");
	});

	$("#poppins-bold").click(function(){
		$(this).addClass("styleon"),
		$("#poppins-italic").removeClass("styleon"),
		$("#poppins-regular").removeClass("styleon"),
		$("#poppins-bold-italic").removeClass("styleon"),
		$("body").addClass("poppins-bold");
		$("body").removeClass("poppins-italic");
		$("body").removeClass("poppins-regular");
		$("body").removeClass("poppins-bold-italic");
	});

	$("#poppins-bold-italic").click(function(){
		$(this).addClass("styleon"),
		$("#poppins-italic").removeClass("styleon"),
		$("#poppins-bold").removeClass("styleon"),
		$("#poppins-regular").removeClass("styleon"),
		$("body").addClass("poppins-bold-italic");
		$("body").removeClass("poppins-italic");
		$("body").removeClass("poppins-bold");
		$("body").removeClass("poppins-regular");
	});
});
