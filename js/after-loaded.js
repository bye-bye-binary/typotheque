/* scripts qui s'appliquent une fois que la page est chargée */

/* ouvrir les liens externes dans des onglets */
$('a[href^="http://"]').not('a[href*=typotheque]').attr('target', '_blank');
$('a[href^="https://"]').not('a[href*=typotheque]').attr('target', '_blank');
$('a[href^="http://"]').not('a[href*=localhost]').attr('target', '_blank');
$('a[href^="https://"]').not('a[href*=localhost]').attr('target', '_blank');


// SLIDING GLYPHS
$(function() {
    //global variables
    var interval, timeout;
    //function to animate the images in forward direction
    var backward = function(event) {
        $el = $(event.target);
        var $top = $el.siblings('.glyphs').find('div.top')
        var $next = $top.next();
        var $first = $el.siblings('.glyphs').find('div').first()

        if (!$next.length) {
            $first.removeClass('invisible');
            $first.addClass('top');
            $top.addClass('invisible');
            $top.removeClass('top');

        } else {
            $next.addClass('top')
            $next.removeClass('invisible')
            $top.addClass('invisible');
            $top.removeClass('top');
        }
    }
    //function to animate the images in backward direction
    var forward = function(event) {
        $el = $(event.target);

        var $top = $el.siblings('.glyphs').find('div.top')
        var $next = $top.prev();
        var $last = $el.siblings('.glyphs').find('div').last()

        if (!$next.length) {
            $last.removeClass('invisible');
            $last.addClass('top');
            $top.addClass('invisible');
            $top.removeClass('top');
        } else {
            $next.addClass('top');
            $next.removeClass('invisible');
            $top.addClass('invisible');
            $top.removeClass('top');
        }
    }
    //capture the front click
    $(document).on('click', '.next', function(event) {
        forward(event);
    });
    //capture the back click
    $(document).on('click', '.prev', function(event) {
        backward(event);
    });

    $('.showroom .texte').attr('contenteditable','true');
    $('.showroom .texte').attr('spellcheck','false');
});
