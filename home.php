<html>
<head>
	<script type="text/javascript" src="<?php echo $config->urls->templates?>scripts/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $config->urls->templates?>scripts/jquery.color-2.1.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $config->urls->templates?>scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo $config->urls->templates?>scripts/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="<?php echo $config->urls->templates?>scripts/jquery.marquee.js"></script>
	<script type="text/javascript" src="<?php echo $config->urls->templates?>scripts/script.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/styles2.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
</head>

<body>

	<style type="text/css">#dragscroll{background-image: url('<?php echo $config->urls->templates?>img/fond.png');}</style>


<?php
// DÉBUT DU PHP

	// On récupère toutes les pages
	$articles = $pages->find("template=article");

	$champ_tags = $fields->get('tags');
	$tags = $champ_tags->type->getOptions($champ_tags);

	$tags_systeme = array();
	echo '<div id="barre_tags" data-tous-actifs="true">';

	for ($i=0; $i<=count($tags); $i++) {
		$tag = $tags[$i];
		$donnees_tag = $tag->data;
		$titre_tag = $donnees_tag["title"];
		$slug_tag = $donnees_tag["value"];

		$tags_systeme[$slug_tag] = $titre_tag;

		if ($titre_tag != NULL) {
			echo "<span class='tag' data-slug='".$slug_tag."' data-actif='true'>".$titre_tag."</span>";
		}
	}
	echo "</div>";


	echo "<div id='dragscroll'>";

	
	// Pour chaque article
	for ($i=0; $i<count($articles); $i++) {
		$article_courant = $articles[$i]; // on récupère la page courante
		$titre = $article_courant->title; // titre de la page courante



		//$position_x = $article_courant->position_x;
		//$position_y = $article_courant->position_y;
		$position_y = rand(130,2850);
		$position_x = rand(50,3300);

		/*$position_y = 0;
		$position_x = 0;*/

		$chemin = substr($article_courant->path, 1); // chemin de la page courante moins le premier caractère

		
		$array_tags = []; // On récupère les tags associés à chaque article // Tableau vide destiné à contenir les tags de l'article
		foreach($article_courant->tags as $tag) { // Boucle pour chaque tag de l'article
			array_push($array_tags, $tag->value); // On rempli le tableau avec le slug de chaque tag de l'article
		}
		$chaine_tags = implode(",", $array_tags); // On converti le tableau de slugs de tags en une chaine de caractère séparée par des virgules




			echo "<div class='article' data-url='".$chemin."' data-tags-slugs='".$chaine_tags."' data-actif='true' style='top:".$position_y."px;left:".$position_x."px;'>"; // imprimer le début de la balise, avec des attributs de données contenant l'url de l'article et les slugs des tags associés

				echo "<div class='titre'>
						<p class='titre_texte'>";
					echo $titre; // imprimer le titre
					echo "</p>
					</div>
					<div class='contenu_fenetre'>
						<div  class='sortie'>
							<div class='premier_bloc'>
								<div class='metadonnees'>
									<ul class='categories'>
										<li><p>Auteurice·s:</p><p id='colonne_deux' class='autrices'></p></li>
										<li><p>Date:</p><p id='colonne_deux' class='date_parution'></p></li>
										<li><p>Pays:</p><p id='colonne_deux' class='pays'></p></li>
										<li><p>Type:</p><p id='colonne_deux' class='type'></p></li>
										<li><p>Source:</p><p id='colonne_deux' class='source'></p></li>";
										$liste_tags = "";
										for ($j=0; $j<count($array_tags); $j++) {
											$liste_tags .= $tags_systeme[$array_tags[$j]].", ";
										}
										$liste_tags = substr($liste_tags, 0, -2);
										echo "<li><p>Thèmes:</p><p id='colonne_deux'>".$liste_tags."</p></li>";
									echo"</ul>
								</div>
								<div class='resume'></div>
							</div>
							<div class='barre_impression'>
								<button class='impr'>IMPRIMER</button><!--
								 --><button class='pdf_src'>DOCUMENT SOURCE</button><!--
								 --></div>
								<div class='texte'>
								</div>
							</div>
						</div>
					</div>"; // fermer la balise

	}


	/////////////////////////////////////
	/////////////////////////////////////
	/////////////////////////////////////

	echo"</div>";

	

// FIN DU PHP
?>



	<div id="logo">
		<img id="logo_img" src="<?php echo $config->urls->templates?>img/logo.png">
	</div>
			<div id="presentation" class="presentation">
				Kratos Gratos<br><br>
				est un infokiosque numérique. Les textes qui y sont publiés sont autant d’outils pour lutter contre les oppressions systémiques. Nous les avons choisis car ils nous touchent dans nos têtes et dans nos corps. Glanés sur le web et dans  “la vraie vie”, nous les rassemblons ici pour les mettre à la disposition de tou·te·s.
			</div>

	<div id="nb_textes">
		<p><span></span></p>
	</div>


<script>
	
	$(document).ready(function() {

		$(window).scrollLeft(0);
		$(window).scrollTop(0);

		$("body").scrollLeft(0);
		$("body").scrollTop(0);


		$('#dragscroll').draggable({

		// create: function() {
	 //  		$(this).css({
	 //  			"top":"-500px",
	 //  			"left":"-500px",
	 //  			"transition":"all ease-out 0.3s",
	 //  			"display":"block"	
	 //  		});

	 //  	},
	  	//  drag: function( event, ui){
	  	//  	let left = ui.position.left;
	  	//  	let top = ui.position.top;

	  	//  	if (left > 0) {
	  	//  		ui.position.left = 0;
	  	//  	} else if (left < -4000 + window.innerWidth) {
	  	//  		ui.position.left = -4000 + window.innerWidth;
	  	//  	}

	  	//  	if (top > 0) {
	  	//  		ui.position.top = 0;
	  	//  	} else if (top < -3000 + window.innerHeight) {
	  	//  		ui.position.top = -3000 + window.innerHeight;
	  	//  	}
  		// }
		});

		$('.article').draggable();

		$(".article").click(function(){
        $(this).toggleClass("article_actif");
        $(".contenu_fenetre", this).slideToggle("fast");
        $(".contenu_fenetre", this).toggleClass("contenu_fenetre_affiche");
        $('.contenu_fenetre_affiche').click(function(){return false;});
    	});



		// NOTES
		function traiter_notes(texte) {
			console.log("ok");
			texte = replaceAll(texte, "{{", "<span class='appel'>†</span><span class='footnote'>");
			texte = replaceAll(texte, "}}", "</span>");
			return texte;
			//$(".contenu_fenetre .texte").html(texte);
		}
    	
		$(document).on("mouseover", ".appel", function(event) {
			let pos_x = event.pageX-5;
			let pos_y = event.pageY-5;
			let contenu_note = $(this).next().html();
			$("body").append("<div class='conteneur_note' style='top:"+pos_y+"px;left:"+pos_x+"px;'>"+contenu_note+"</div>")
		});

		$(document).on("mouseout", ".conteneur_note", function() {
			$(this).remove();
		});

		function replaceAll(str, find, replace) {
		    return str.replace(new RegExp(find, 'g'), replace);
		}
		// NOTES FIN


		//BOUTONS IMPRESSIONS
		$('.impr, .pdf_src').mousedown(function(){
  			//alert("IMPRESSION");
  			if ($(this).attr("data-fichier") != "null") {
  				let fichier = "http://xn--eugniebidaut-deb.eu"+$(this).attr("data-fichier");
  				window.open(fichier, '_blank');
  			}
  			
		});

		$('.impr_sans_img').mousedown(function(){
  			alert("IMPRESSION SANS IMAGES");
		});

		// $('.pdf_src').mousedown(function(){
  // 			alert("VOIR PDF SOURCE");
		// });
		//BOUTONS IMPRESSIONS FIN



		let url_site = "<?php echo $config->urls->httpRoot; ?>";
		let tags_actifs = [];

		// FONCTION QUI GÈRE L'AFFICHAGE DES ARTICLES EN FONCTION DES TAGS
		
		function update_tags_actifs() {
			tags_actifs = [];
			$(".tag[data-actif='true']").each(function() {
				let slug = $(this).attr("data-slug");
				tags_actifs.push(slug);
			});
		}


		function tags_articles() {
			// POUR CHAQUE ARTICLE, ON COMPARE SES TAGS AU TABLEAU DE TAGS ACTIFS


			var nombre_articles_actifs = 1;

			$(".article").each(function() {
				let tags_article = $(this).attr("data-tags-slugs");
				$(this).attr("data-actif", "false");

				for (var i=0; i<tags_actifs.length; i++) {
					let tag_actif = tags_actifs[i];
					if (tags_article.indexOf(tag_actif) != -1) {
						$(this).attr("data-actif", "true");
						nombre_articles_actifs++;
						break;
					}

				$("#nb_textes p span").html(nombre_articles_actifs+" textes");
				}
			});
		}


		$(".article").click(function() {
			let url_article = $(this).attr("data-url");
			let article_clicke = $(this);
			$.getJSON(url_site+url_article, function(data) {
				console.log(data);
				article_clicke.find(".date_parution").html(data.date_parution);
				article_clicke.find(".autrices").html(data.autrices);
				article_clicke.find(".pays").html(data.pays);
				article_clicke.find(".type").html(data.type);
				article_clicke.find(".source").html(data.source);
				article_clicke.find(".resume").html(data.resume);
				article_clicke.find(".texte").html(traiter_notes(data.texte));
				article_clicke.find(".impr").attr("data-fichier", data.document_a_imprimer);
				article_clicke.find(".pdf_src").attr("data-fichier", data.document_source);
				
			});
		});



		$(".tag").click(function() {

			let etat_tag = $(this).attr("data-actif");
			let etat_tags = $("#barre_tags").attr("data-tous-actifs");


			if (etat_tag == "true" && etat_tags == "false") {
				$(".tag").each(function() {
					$(this).attr("data-actif", "true");
				});
				$("#barre_tags").attr("data-tous-actifs", "true");
			} else {
				$(".tag").each(function() {
					$(this).attr("data-actif", "false");
				});
				$(this).attr("data-actif", "true");
				$("#barre_tags").attr("data-tous-actifs", "false");
			}


			update_tags_actifs();
			tags_articles();
		});

	
	});

</script>


</body>
</html>