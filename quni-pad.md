## Pratiques en commun, de normes molles, rageuses

Les fontes de la typothèque proposent de nombreux glyphes (lettres mutantes) en addition au point médian et autres solutions régulièrement utilisées pour écrire et composer des textes inclusifs. Nos claviers ne contiennent pas (encore) les touches qui correspondent à ces caractères et ces ligatures. Alors pour rendre utilisable cet arc-en-ciel de signes, nous sommes en train de construire un ensemble de tours de main, des pratiques en commun, de normes molles, rageuses et aux petits oignons, qui ensemble forment læ Queer Unicode Initiative (QUNI). 

![Exemple Baskervvol](
https://safe.genderfluid.space/apps/files_sharing/publicpreview/9iBKRKbw8CqxCmy?x=1920&y=605&a=true&file=2021_QUNI_baskervvol_img1.png&scalingup=0)
ou 
#![Exemple Baskervvol](https://safe.genderfluid.space/apps/files_sharing/publicpreview/kRFoTKSWfG5reQt?x=1920&y=623&a=true&file=2021_QUNI_baskervvol_img2.png&scalingup=0)


## Le standard Unicode comme terrain vague

Cette grande prairie de jeux et d'expériences inclusives est aménagée à l'intérieur de la Supplementary Private Use Area-A (PUA-A) (https://en.wikipedia.org/wiki/Private_Use_Areas#PUA-A) du très industriel et latino-centré standard Unicode (https://fr.wikipedia.org/wiki/Unicode). On n'a pas le choix, c'est le "leading standard" qui est utilisé par tous les téléphones, ordinateurs et logiciels qui nous entourent. Pour y injecter le QUNI qu'il mérite, on campe dans un bloc terrain vague en bordure, et c'est évidement la zone la plus cool. Nous nous sommes inspirés du [Medieval Unicode Font Initiative (MUFI)](https://mufi.info/), un projet visant à coordonner l’encodage et l’affichage de caractères médiévaux écrits en alphabet latin.

#![Vue sur le tableau du mapping](https://safe.genderfluid.space/apps/files_sharing/publicpreview/S2gKfW7Ha5sXnnG?x=1920&y=623&a=true&file=2021_QUNI_mapping_pua_img1.png&scalingup=0)
ou 
#![Vue sur le tableau du mapping](https://safe.genderfluid.space/apps/files_sharing/publicpreview/gwao8ZCxrFcH3iq?x=1920&y=623&a=true&file=2021_QUNI_mapping_pua_img2.png&scalingup=0)


## Pour qui? Pour plein de nous!

Læ QUNI est développæ

* pour que les utilisatrix des fontes puissent accéder et utiliser les glyphes inclusifves dans leurs textes d'une manière compatible entre différentes fontes;

[image - vue sur un même bout de texte composés avec deux fontes qunifiées] https://safe.genderfluid.space/s/Kji2zbcr2AHp8KK

* pour que les dessinatrix des fontes puissent poser les glyphes dans les cases unicodes communes; 

[image de case unicode] Je comprends pas trop celle-ci - je m'en occupe (Pierre)

* pour que les mêmes dessinatrix des fontes puissent utiliser les fonctionalités opentype (features) déjà codées, ou ajouter les leurs selon une logique partagée.

[image de bout de code features] https://safe.genderfluid.space/s/sLeKkgymzEoxaay

Qunifier une fonte est aisé, on a identifié 48 formes de glyphes qui couvre la plupart des besoins. Il suffit de les dessiner et de les ajouter à votre fonte selon l'encodage du QUNI. Voir notre screencast de 4 minutes pour qunifier votre fonte avec Glyphs ou avec FontForge!

[screenshots des deux screencasts]

Pour aller un peu plus loin, nous avons répertoriés plusieurs modes de fonctionnement des fontes inclusives existantes :

### Ligatures
xxxx
[schéma - diagramme - exemple de quelques glyphes dans une fonte concernée]

### Formes alternatives
xxxxx
[schéma - diagramme - exemple de quelques glyphes dans une fonte concernée]

### Signes diacritiques
xxxxxx
[schéma - diagramme - exemple de quelques glyphes dans une fonte concernée]

Le QUNI est prêt à de très nombreux ajouts, c'est une initiative blobesque, et au 19 octobre 2021, ce sont 154 signes ou groupes de glyphes inclusifs qui sont déjà prêts à être farçis de dessins de lettres. Et de plus en plus de fontes se font garnir de ces lettres additionnelles en mode QUNI. Chacun de ces signes peut être enrichi de quinze variantes de dessin, et ça dans chaque fonte. L'initiative a des perspectives d'agrandissement, puisqu'en tout ce sont 3496 cases qui sont disponibles pour y injecter de nouvelles propositions de glyphes, et 52440 variantes!

[schéma - diagramme - vue sur le tableau version PUA]

Dans https://www.arllfb.be/actualite/ecriture_inclusive.pdf des détracteurs de l'écriture inclusive avancent que
> "L’Académie rappelle les deux caractères fondamentaux du signe linguistique : l’arbitraire et la linéarité. Les conséquences de cet arbitraire sont que la langue ne représente pas directement le réel et ne détermine pas la pensée, dans la mesure où les locuteurs d’une même langue peuvent exprimer des conceptions très différentes. Quant à la linéarité, certaines pratiques de l’écriture inclusive, dont le point médian et les « néologismes morphologiques » créés par amalgames (celleux), exigent un décryptage empêchant une lecture linéaire essentielle à la compréhension d’un texte (exemple : tou·te·s ou tou·t·es sénateur·rice·s, usant de segments inexistants). L’Académie se prononce clairement contre ces formes contre-intuitives et très instables."
Nous pensons que cet arbitraire peut aussi être développé dans la typographie, et que les signes peuvent être dessinés par "amalgames morphologiques" pour agir sur la fluidité, d'une manière volontairement positive, ou volontairement négative et ça a été le propre de la typographie, et du lettrage, de l'écriture, et cela même avant l'invention des caractères mobiles. De plus, l'impact du langage sur la pensée est complètement admis en linguistique et en psychologie (hypothèse Sapir-Whorf), et les recherches récentes en morphologie linguistique tendent à montrer que les signes linguistiques sont motivés (ou en tout cas pas complètement arbitraires).

* [Link](https://journals.openedition.org/edl/2376#bodyftn5)
* [Les motivations du signe linguistique : vers une typologie des relations signifiant/signifié](http://cle.ens-lyon.fr/plurilangues/langue/miscellanees/les-motivations-du-signe-linguistique-vers-une-typologie-des-relations-signifiant-signifie
* [La motivation du nom face à l'arbitraire du signe : à propos de Shoah](https://www.persee.fr/doc/hispa_0007-4640_2005_num_107_1_5232


## Deux usages du QUNI

Il y a deux usages du QUNI, pour vous ↓ utilisatrix des fontes, et pour vous ⇊ dessinatrix des fontes.

Un : pour utiliser les fontes de la typothèque dans vos textes, voir les ligatures dispon glyphes post-binaires sont disponibles pour chaque fonte. Xxxxxxxx xxxxxx xxxxxxxx.

Un manuel d'utilisation des glyphes, surtout axé sur la big question "on y accède comment?" et trié logiciel par logiciel, est disponible ci-contre xxxxx xxxxxxxxx xxxxxx.

Si vous ne comprenez rien, pas de stress, BBB ne recule devant aucune difficulté et propose une hotline des workshops de prise en main xxxxxxxxxxxxx xxxxxxx xxxxxxxxxxxxxx xxxxxxxxxxx.

Deux : pour dessiner des glyphes inclusifs dans des fontes, le QUNI est construit autour de trois éléments-outils :
* quoi : la liste des glyphes proposés à dessiner;
* où les mettre : le tableau des point de codage Unicode qui y correspondent;
* comment les faire apparaitre (y accéder) : les fonctionnalités opentype avec lesquelles équiper les fontes

**Quoi**, la liste des glyphes reprend le max de glyphes qui ont été injectés par les typographes des premières fontes de la typothèque. Ce sont très largement des glyphes utilisés dans le français. C'est une liste très ouverte, elle va certainement être enrichie pour encore environ dix millénaires (au moins l'âge du patriarcat). Xxxxxxxx xxxxxx xxxxxxxx xxxxxx.

**Où**, le tableau des point de codage permet de savoir dans quelle case mettre quel glyphe. Le fait d'avoir choisi de profiter de la PUA permet d'avoir à la fois des noms de glyphes (exemple: eacute_e), utilisés entre autres dans les features OpenType, mais aussi des codes unicode (U+....) utilisables dans les logiciels rustiques ou, dans certains cas, sur le web. En gros, cela permet d'être le plus largement compatible. 
[schéma]
redesign du tableau avec différents onglets
48 glyphes qui couvrent tous les suffixes
Il y a des formes plus fondues (colorées dans les 2 tabl)
Les noires isolées qu'on peut pimper
orange et rouge = chantier
supprimer la colonne E

Les variantes possibles pour chaque glyphe utilisent les codes xxxxxx xxxxxxx.
// maybe paragraphe différent pour expliquer le fonctionnement de l'encodage des formes alternatives
[schéma]

**Comment**, les features OpenType permettent d'appeler les glyphes inclusifs grâce à un système de remplacements automatiques. Ainsi il suffit à l'utilisatrix de la fonte de rédiger en inclusif en utilisant le point médian pour appeler le glyphe adéquat. Par exemple é·e est automatiquement remplacé par le glyphe "eacute_e".
[schéma]
Elles sont documentées le plus largement possible dans læ QUNI mais on peut déjà dire que xxxxxxxxxxxxx xxxxxxx xxxxxxxxxxxxxx xxxxxxxxxxx.

Si vous avez des fontes que vous voulez proposer pour être farcies de nouveaux glyphes ou que xxxxxxxxxxxxx xxxxxxx xxxxxxxxxxxxxx xxxxxxxxxxx, n'hésitez pas!